# README #

This is a BASH script created for Centos Linux but can easily be adapted to run on any Unixoid server. It is meant to help newcomers to web development (and lazy veterans ;-)to configure a VPS. The script creates a new user account, defines the domain name, creates the appropriate record in hosts file, configures other files and permissions for Apache and, finally, creates a MySQL database and a corresponding user account. Have fun with your new VPS!
