#  account_gen.sh
#  
#  Copyright 2014 Georgios Tsotsos <tsotsos@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  

#!/bin/bash
HTTPDCONF='/etc/httpd/conf/httpd.conf';
HOSTS='/etc/hosts';
if [ ! -f $HTTPDCONF ]; then
    #TODO: Check this for other distributions than openSUSE
    # HTTPDCONF=$(ps -ef |grep apache |awk '{print $10}' | head -n 1);
    read -p "Please enter the full path of your apache configuration file :" HTTPDCONF
fi
if [ ! -f $HOSTS ]; then
    read -p "Please enter the full path of your apache configuration file :" HOSTS
fi
MYIP=$(ip route get 8.8.8.8 | awk 'NR==1 {print $NF}');
APACHEUSER=$(ps -ef |grep apache |awk '{print $1}' | head -n 1)

DOCROOT='/var/www';
MYSQL='mysqld'; #TODO: change that with a regext to detect the actual mysql service.
if ( ps ax | grep -v grep | grep $HTTPD > /dev/null ); then 
    echo "$HTTPD service running......OK"
    HTTPDON=1
    HTTPD=""
else
    echo "$HTTPD is not running.......NO"
    HTTPDON=0
fi;
if [ $MYSQLON -eq 0 ] && [ $HTTPDON -eq 0 ]; then 
	echo -e "Please install and/or enable the following services:\n $HTTPD \n $MYSQL";
	exit 0
fi;
echo -e "Please folow the instructions to crete user, password, mysql database and domain name.\n";
echo -e "------------------------------------------------------------------------\n";
if [ $(id -u) -eq 0 ]; then
	read -p "Enter the username :" username
	read -s -p "Enter the password :" password
	echo -e "\n"
	read -p "Enter the desired domain name:" domain
	egrep "^$username" /etc/passwd >/dev/null
if [ $? -eq 0 ]; then
	echo "$username exists!\n Try again with other username"
	exit 1
else
	pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)
	useradd -m -p $pass $username
	if [ $? -eq 0 ]; then
		mkdir -p $DOCROOT/$username/public_html
		mkdir -p $DOCROOT/$username/logs/
		chown -R $username:$APACHEUSER $DOCROOT/$username/
		chmod -R 6755 $DOCROOT/$username/
		echo "User and directories created.......OK"
cat >> $HTTPDCONF << EOF
<VirtualHost *:80>
	ServerAdmin $username@$domain
	DocumentRoot $DOCROOT/$username/public_html
	ServerName $domain
	ErrorLog $DOCROOT/$username/logs/$username.error.log
	<Directory "$DOCROOT/$username/public_html">
		AllowOverride All
	</Directory>
</VirtualHost>
EOF
	echo "$MYIP	$domain" >> $HOSTS
	else
		echo "Failed to add a user!"
		exit 2
	fi;
fi;
fi;
read -p "Create MySQL database? (y/n)" yn
case $yn in
y|Y )
	if [ -z "$rootpasswd" ]; then
		echo "Please enter MySQL root user password!"
		read -s rootpasswd
	fi;
	echo "Creating a new database.... called : $username"
	/usr/bin/mysql -uroot -p$rootpasswd -e "CREATE DATABASE $username;"
	echo "Database created............OK"
	/usr/bin/mysql -uroot -p$rootpasswd -e "CREATE USER $username@localhost IDENTIFIED BY '$password';"
	echo "User created................OK"
	/usr/bin/mysql -uroot -p$rootpasswd -e "GRANT ALL PRIVILEGES ON $username.* TO '$username'@'localhost';"
	/usr/bin/mysql -uroot -p$rootpasswd -e "FLUSH PRIVILEGES;"
	echo "Permissions Grant...........OK"
	/usr/bin/mysql -uroot -p$rootpasswd -e "show databases;"
	;;
n|N )
	exit;
esac
echo "Thank you... have a nice day!"
service $HTTPD restart
